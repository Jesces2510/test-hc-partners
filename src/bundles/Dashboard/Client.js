/* eslint-disable no-script-url */

import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import { BrowserRouter as Router, Route } from "react-router-dom";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

import Title from "./Title";
import { ticketsByUser, ticketClaim } from "../../service/services";

const useStyles = theme => ({
    seeMore: {
        marginTop: "24px"
    },
    cellAction: {
        width: "20%"
    }
});

class Client extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todo: {},
            idEdit: ""
        };
    }

    componentDidMount() {
        let idUser = localStorage.getItem("idUser");
        ticketsByUser(idUser).then(res => {
            this.setState({ todo: res.data.data });
        });
    }

    isStatusOk(status) {
        if (status === "0") {
            return <TableCell>Pendiente</TableCell>;
        } else {
            return <TableCell>Pedido</TableCell>;
        }
    }

    handleClickClaimTicket(id) {
        let idUser = localStorage.getItem("idUser");
        let data = {
            id: id,
            idUser: idUser
        };
        ticketClaim(data).then(res => {
            if (res.data.success === "OK") {
                this.setState({ todo: res.data.data });
            }
        });
    }

    isEnableButton(status, id) {
        if (status === "0") {
            return (
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.handleClickClaimTicket(id)}
                >
                    Solicitar Ticket
                </Button>
            );
        } else {
            return (
                <Button variant="contained" color="primary" disabled>
                    Solicitar Ticket
                </Button>
            );
        }
    }

    tabRow() {
        const spacingButtons = {
            margin: "2%"
        };

        if (this.state.todo.length) {
            return this.state.todo.map(row => (
                <TableRow key={row.id}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.name}</TableCell>
                    {this.isStatusOk(row.status)}
                    <TableCell>
                        {this.isEnableButton(row.status, row.id)}
                    </TableCell>
                </TableRow>
            ));
        }
    }

    renderTickets() {
        const useStyles = theme => ({
            seeMore: {
                marginTop: "24px"
            },
            cellAction: {
                width: "20%"
            }
        });
        const styles = useStyles();
        return (
            <React.Fragment>
                <Title>Tickets Cliente</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Nombre</TableCell>
                            <TableCell>Estado</TableCell>
                            <TableCell style={styles.cellAction}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>{this.tabRow()}</TableBody>
                </Table>
            </React.Fragment>
        );
    }

    render() {
        return (
            <Router>
                <Route
                    exact
                    path="/Dashboard"
                    render={() => this.renderTickets()}
                />
            </Router>
        );
    }
}

Client.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(useStyles)(Client);
