/* eslint-disable no-script-url */

import React from "react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

import Title from "./Title";
import Tickets from "./Tickets";
import { users, ticket, ticketEdit } from "../../service/services";

const useStyles = theme => ({
    seeMore: {
        marginTop: "24px"
    },
    cellAction: {
        width: "20%"
    }
});

class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: {},
            id: this.props.idEdit,
            values: {
                id: ""
            }
        };
    }

    componentDidMount() {
        users().then(res => {
            this.setState({ users: res.data.data });
        });

        ticket(this.state.id).then(res => {
            if (res.data.success === "OK") {
                this.setState({ values: { id: res.data.data.id_user } });
            }
        });
    }

    handleClickBack() {
        this.setState({ toTickets: true });
    }

    getUsers() {
        if (this.state.users.length) {
            return this.state.users.map(row => (
                <MenuItem value={row.id} key={row.id}>
                    {row.name}
                </MenuItem>
            ));
        }
    }

    renderTickets() {
        return <Tickets />;
    }

    handleChange(event) {
        this.setState({ values: { [event.target.name]: event.target.value } });
    }

    hadnleClickEditTicket() {
        if (this.state.values.id !== "") {
            let data = {
                idTicket: this.state.id,
                id: this.state.values.id
            };
            ticketEdit(data).then(res => {
                if (res.data.success === "OK") {
                    this.setState({ toTickets: true });
                }
            });
        }
    }

    renderEdit() {
        const useStyles = theme => ({
            seeMore: {
                marginTop: "24px"
            },
            cellAction: {
                width: "20%"
            },
            root: {
                display: "flex",
                flexWrap: "wrap"
            },
            formControl: {
                margin: "1%"
            },
            selectEmpty: {
                marginTop: "20%"
            },
            textField: {
                marginLeft: "8px",
                marginRight: "8px",
                width: 300
            },
            menu: {
                width: 300
            }
        });
        const styles = useStyles();
        return (
            <React.Fragment>
                <Title>Editar Ticket</Title>
                <form style={styles.root} autoComplete="off">
                    <FormControl style={styles.formControl}>
                        <Grid container spacing={4}>
                            <Grid item xs={12} sm={12}>
                                <TextField
                                    id="userselect"
                                    select
                                    label="Selecciona un usuario"
                                    style={styles.textField}
                                    value={this.state.values.id}
                                    onChange={this.handleChange.bind(this)}
                                    SelectProps={{
                                        MenuProps: {
                                            style: styles.menu
                                        }
                                    }}
                                    inputProps={{
                                        name: "id",
                                        id: "id"
                                    }}
                                    margin="normal"
                                >
                                    {this.getUsers()}
                                </TextField>
                            </Grid>
                        </Grid>
                    </FormControl>
                </form>
                <div style={styles.seeMore}>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={1}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => this.hadnleClickEditTicket()}
                            >
                                Editar
                            </Button>
                        </Grid>
                        <Grid item xs={12} sm={1}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => this.handleClickBack()}
                            >
                                Atrás
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </React.Fragment>
        );
    }

    renderRedirectTickets() {
        if (this.state.toTickets) {
            return <Redirect to="/Dashboard" />;
        }
    }

    render() {
        return (
            <Router>
                {this.renderRedirectTickets()}
                <Route
                    exact
                    path="/Dashboard"
                    render={() => this.renderTickets()}
                />
                <Route exact path="/Edit" render={() => this.renderEdit()} />
            </Router>
        );
    }
}

Edit.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(useStyles)(Edit);
