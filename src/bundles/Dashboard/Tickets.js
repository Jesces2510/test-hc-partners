/* eslint-disable no-script-url */

import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutlined";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

import Title from "./Title";
import { tickets, ticketDel } from "../../service/services";
import Create from "./Create.js";
import Edit from "./Edit.js";

const useStyles = theme => ({
    seeMore: {
        marginTop: "24px"
    },
    cellAction: {
        width: "20%"
    }
});

class Tickets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todo: {},
            toCreate: false,
            toEdit: false,
            idEdit: ""
        };
    }

    componentDidMount() {
        tickets().then(res => {
            this.setState({ todo: res.data.data });
        });
    }

    isStatusOk(status) {
        if (status === "0") {
            return <TableCell>Pendiente</TableCell>;
        } else {
            return <TableCell>Pedido</TableCell>;
        }
    }

    handleClickAdd() {
        this.setState({ toCreate: true });
    }

    handleClickEdit(id) {
        this.setState({ toEdit: true, idEdit: id });
    }

    handleClickDelete(id) {
        let data = {
            id: id
        };
        ticketDel(data).then(res => {
            if (res.data.success === "OK") {
                this.setState({ todo: res.data.data });
            }
        });
    }

    tabRow() {
        const spacingButtons = {
            margin: "2%"
        };

        if (this.state.todo.length) {
            return this.state.todo.map(row => (
                <TableRow key={row.id}>
                    <TableCell>{row.id}</TableCell>
                    <TableCell>{row.name}</TableCell>
                    {this.isStatusOk(row.status)}
                    <TableCell>
                        <Button
                            variant="contained"
                            color="secondary"
                            style={spacingButtons}
                            onClick={() => this.handleClickDelete(row.id)}
                        >
                            <DeleteOutlinedIcon />
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            style={spacingButtons}
                            onClick={() => this.handleClickEdit(row.id)}
                        >
                            <EditOutlinedIcon />
                        </Button>
                    </TableCell>
                </TableRow>
            ));
        }
    }

    renderRedirectCreate() {
        if (this.state.toCreate) {
            return <Redirect to="/Create" />;
        }
    }

    renderRedirectEdit() {
        if (this.state.toEdit) {
            return <Redirect to="/Edit" />;
        }
    }

    renderCreate() {
        return <Create />;
    }

    renderEdit() {
        return <Edit idEdit={this.state.idEdit} />;
    }

    renderTickets() {
        const useStyles = theme => ({
            seeMore: {
                marginTop: "24px"
            },
            cellAction: {
                width: "20%"
            }
        });
        const styles = useStyles();
        return (
            <React.Fragment>
                <Title>Tickets</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Nombre</TableCell>
                            <TableCell>Estado</TableCell>
                            <TableCell style={styles.cellAction}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>{this.tabRow()}</TableBody>
                </Table>
                <div style={styles.seeMore}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => this.handleClickAdd()}
                    >
                        <AddOutlinedIcon /> Crear
                    </Button>
                </div>
            </React.Fragment>
        );
    }

    render() {
        return (
            <Router>
                {this.renderRedirectCreate()}
                {this.renderRedirectEdit()}
                <Route
                    exact
                    path="/Create"
                    render={() => this.renderCreate()}
                />
                <Route exact path="/Edit" render={() => this.renderEdit()} />
                <Route
                    exact
                    path="/Dashboard"
                    render={() => this.renderTickets()}
                />
            </Router>
        );
    }
}

Tickets.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(useStyles)(Tickets);
