import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import { SignIn } from "./bundles/SignIn/SignIn";
import { SignUp } from "./bundles/SignUp/SignUp";
import { Dashboard } from "./bundles/Dashboard/Dashboard";
import { signUp, signIn } from "./service/services";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toSignIn: false,
            toDashboard: false,
            idProfile: false,
            tickets: {}
        };
    }

    handleClickRegister() {
        const data = {
            name:
                document.getElementById("firstName").value +
                " " +
                document.getElementById("lastName").value,
            email: document.getElementById("email").value,
            password: document.getElementById("password").value
        };
        signUp(data).then(res => {
            if (res.data.success === "OK") {
                this.setState({ toSignIn: true });
            } else {
                alert(res.data.data);
            }
        });
    }

    handleClickLogin() {
        const data = {
            email: document.getElementById("email").value,
            password: document.getElementById("password").value
        };
        signIn(data).then(res => {
            if (res.data.success === "OK") {
                localStorage.setItem("idProfile", res.data.data.idProfile);
                localStorage.setItem("idUser", res.data.data.idUser);
                this.setState({ toDashboard: true });
            } else {
                alert(res.data.data);
            }
        });
    }

    renderSignUp() {
        return <SignUp onClick={() => this.handleClickRegister()} />;
    }

    renderSignIn() {
        return <SignIn onClick={() => this.handleClickLogin()} />;
    }

    renderDashboard() {
        return <Dashboard />;
    }

    renderRedirectSignIn() {
        if (this.state.toSignIn) {
            return <Redirect to="/" />;
        }
    }

    renderRedirectDashboard() {
        if (this.state.toDashboard) {
            return <Redirect to="/Dashboard" />;
        }
    }

    render() {
        return (
            <Router>
                {this.renderRedirectSignIn()}
                {this.renderRedirectDashboard()}
                <Route exact path="/" render={() => this.renderSignIn()} />
                <Route
                    exact
                    path="/SignUp"
                    render={() => this.renderSignUp()}
                />
                <Route
                    exact
                    path="/Dashboard"
                    render={() => this.renderDashboard()}
                />
                <Route
                    exact
                    path="/Create"
                    render={() => this.renderDashboard()}
                />
            </Router>
        );
    }
}

// ========================================

ReactDOM.render(<Main />, document.getElementById("root"));
