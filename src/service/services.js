import Axios from "axios";

function signUp(data) {
    return Axios.post("localhost/test-hc-partners-back/api/signup", data);
}

function signIn(data) {
    return Axios.post("localhost/test-hc-partners-back/api/signin", data);
}

function users() {
    return Axios.get("localhost/test-hc-partners-back/api/users");
}

function tickets() {
    return Axios.get("localhost/test-hc-partners-back/api/tickets");
}

function ticketsByUser(id) {
    return Axios.get("localhost/test-hc-partners-back/api/ticketsbyuser/" + id);
}

function ticket(id) {
    return Axios.get("localhost/test-hc-partners-back/api/ticket/" + id);
}

function ticketNew(data) {
    return Axios.post("localhost/test-hc-partners-back/api/ticketnew", data);
}

function ticketClaim(data) {
    return Axios.post("localhost/test-hc-partners-back/api/ticketclaim", data);
}

function ticketEdit(data) {
    return Axios.post("localhost/test-hc-partners-back/api/ticketedit", data);
}

function ticketDel(data) {
    return Axios.post("localhost/test-hc-partners-back/api/ticketdel", data);
}

export {
    signUp,
    signIn,
    tickets,
    users,
    ticketNew,
    ticketDel,
    ticket,
    ticketEdit,
    ticketsByUser,
    ticketClaim
};
